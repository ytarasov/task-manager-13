package ru.t1.ytarasov.tm.api.service;

import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task add(Task task);

    Task create(String name);

    Task create(String name, String description);

    void clear();

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    List<Task> findAllTasksByProjectId(String projectId);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeProjectStatusById(String id, Status status);

    Task changeProjectStatusByIndex(Integer index, Status status);

    Task removeById(String id);

    Task removeByIndex(Integer index);
}
