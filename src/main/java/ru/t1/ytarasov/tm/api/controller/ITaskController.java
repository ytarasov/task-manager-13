package ru.t1.ytarasov.tm.api.controller;

import ru.t1.ytarasov.tm.model.Task;

public interface ITaskController {

    void createTask();

    void clearTask();

    void showTasks();

    void showTask(Task task);

    void showTaskById();

    void showTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void findTasksByProjectId();

    void removeTaskById();

    void removeTaskByIndex();
}
