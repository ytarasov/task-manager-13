package ru.t1.ytarasov.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String taskId, String projectId);

    void removeProjectById(String id);

    void unbindTaskFromProject(String taskId, String projectId);

}
