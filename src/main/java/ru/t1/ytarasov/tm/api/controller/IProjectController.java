package ru.t1.ytarasov.tm.api.controller;

import ru.t1.ytarasov.tm.model.Project;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

    void showProject(Project project);

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void startProjectById();

    void startProjectByIndex();

    void completeProjectById();

    void completeProjectByIndex();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void removeById();

    void removeByIndex();
}
