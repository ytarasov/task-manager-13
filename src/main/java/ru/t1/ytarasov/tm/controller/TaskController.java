package ru.t1.ytarasov.tm.controller;

import ru.t1.ytarasov.tm.api.controller.ITaskController;
import ru.t1.ytarasov.tm.api.service.ITaskService;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Task;
import ru.t1.ytarasov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void clearTask() {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        List<Task> tasks = taskService.findAll();
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void showTask(Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId() + ";");
        System.out.println("Name: " + task.getName() + ";");
        System.out.println("Description: " + task.getDescription() + ";");
        System.out.println("Status: " + Status.toName(task.getStatus()));
        System.out.println("Project ID: " + task.getProjectId());
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateById(id, name, description);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[TASK UPDATED]");
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByIndex(index, name, description);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[TASK UPDATED]");
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeProjectStatusById(id, Status.IN_PROGRESS);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.changeProjectStatusByIndex(index, Status.IN_PROGRESS);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void completeTaskById() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeProjectStatusById(id, Status.COMPLETED);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.changeProjectStatusByIndex(index, Status.COMPLETED);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[CHANGE STATUS OF THE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = taskService.changeProjectStatusById(id, status);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[CHANGE STATUS OF THE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = taskService.changeProjectStatusByIndex(index, status);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void findTasksByProjectId() {
        System.out.println("FIND ALL TASKS BY PROJECT ID");
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        List<Task> taskList = taskService.findAllTasksByProjectId(projectId);
        for (Task task : taskList) System.out.println(task);
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[TASK REMOVED]");
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[TASK REMOVED]");
        showTask(task);
        System.out.println("[OK]");
    }

}
